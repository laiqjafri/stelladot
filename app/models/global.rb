class Global < ActiveRecord::Base
  serialize :value
  validates :key,
    :presence => {:message => "Key is required"},
    :uniqueness => {:message => "Key already exists"},
    :length => {:minimum => 2, :message => "Key should at least be 2 characters long"}

  before_save  :confirm_value_type
  after_commit :flush_cache

  def confirm_value_type
    if self.is_b?
      self.value = self.value.present?
    elsif self.is_i?
      self.value = self.value.to_i
    elsif self.is_f?
      self.value = self.value.to_f
    else
      self.value = self.value.to_s
    end
    true
  end

  def is_i?
    return false if self.value.blank?
    !!(self.value.to_s =~ /\A[-+]?[0-9]+\z/)
  end

  def is_f?
    return false if self.value.blank?
    !!(self.value.to_s =~ /^\s*[+-]?((\d+_?)*\d+(\.(\d+_?)*\d+)?|\.(\d+_?)*\d+)(\s*)$/)
  end

  def is_b?
    !!(self.key.to_s =~ /.*\?\z/)
  end

  def type_name
    case self.value.class.name
    when "Fixnum"
      "Integer"
    when "Float"
      "Float"
    when "TrueClass", "FalseClass"
      "Boolean"
    when "String"
      "String"
    else
      "NotAllowed"
    end
  end

  def self.find_from_cache id
    Rails.cache.fetch(["global", id]) { find_by_id id }
  end

  def flush_cache
    Rails.cache.delete(["global", id])
  end
end
