class GlobalsController < ApplicationController
  def index
    @globals = Global.paginate :per_page => 25, :page => params[:page]
  end

  def new
    @global = Global.new
  end

  def edit
    @global = Global.find_from_cache params[:id]
    @global.value = nil if @global.is_b? && !@global.value
  end

  def create
    @global = Global.new global_params
    if @global.save
      flash[:notice] = "Global created successfully"
      return redirect_to(globals_path)
    else
      flash[:error] = @global.errors.messages.values.join("<br />")
      return render(:action => :new)
    end
  end

  def update
    @global = Global.find_from_cache params[:id]
    if @global.update_attributes global_params
      flash[:notice] = "Global saved successfully"
      return redirect_to(globals_path)
    else
      flash[:error] = @global.errors.messages.values.join("<br />")
      return render(:action => :edit)
    end
  end

  def destroy
    @global = Global.find_from_cache params[:id]
    if @global.destroy
      flash[:notice] = "Global deleted successfully"
    else
      flash[:notice] = "Global could not be deleted"
    end
    return redirect_to(globals_path)
  end

  private

  def global_params
    return params.require(:global).permit(:key, :value)
  end
end
