class CreateGlobals < ActiveRecord::Migration
  def change
    create_table :globals do |t|
      t.string :key,   :null => false
      t.string :value
      t.timestamps
    end

    add_index :globals, :key, :unique => true
  end
end
