require 'test_helper'

class GlobalTest < ActiveSupport::TestCase
  test "should save in string format" do
    g = Global.create :key => 'welcome_message', :value => "Welcome to Stelladot App"
    g = Global.where("key = ?", 'welcome_message').first
    assert_equal "String", g.value.class.name
  end

  test "should save in integer format" do
    g = Global.create :key => 'total_staff', :value => 100
    g = Global.where("key = ?", 'total_staff').first
    assert_equal "Fixnum", g.value.class.name
  end

  test "should save in floating point format" do
    g = Global.create :key => 'pi', :value => 3.14
    g = Global.where("key = ?", 'pi').first
    assert_equal "Float", g.value.class.name
  end

  test "should save in boolean format" do
    g = Global.create :key => 'interesting?', :value => true
    g = Global.where("key = ?", 'interesting?').first
    assert_equal "TrueClass", g.value.class.name

    g = Global.create :key => 'difficult?', :value => false
    g = Global.where("key = ?", 'difficult?').first
    assert_equal "FalseClass", g.value.class.name
  end

  test "database validations" do
    g = Global.new :key => 'exists', :value => true
    assert g.valid?
    g.save

    g = Global.new :value => "Requires Key"
    assert_not g.valid?

    g = Global.new :key => 'exists', :value => true
    assert_not g.valid?
  end

  test "should set and return correct type" do
    Global.create :key => 'int_with_string_val', :value => "20000"
    g = Global.where("key = ?", 'int_with_string_val').first
    assert_equal "Integer", g.type_name
  end
end
