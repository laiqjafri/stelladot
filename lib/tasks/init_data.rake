namespace :init_data do
  desc "Add Globals"
  task :globals => :environment do
    Global.create :key => "interesting?", :value => true
    Global.create :key => "difficult?"
    Global.create :key => "pi", :value => 3.14
    Global.create :key => "info_email", :value => "info@stelladot.com"
    Global.create :key => "staff_count", :value => 100
  end

  desc "Run all init data tasks"
  task :all => [:globals]
end
